input AddFileInput {
  id: ID!
  projectID: ID!
  name: String!
  text: TextContent
  clientMutationId: String
}

type AddFilePayload {
  parent: File
  insertedFile: File
  clientMutationId: String
}

type AppState {
  currentProject: Project!
  openingFiles: [File!]!
}

type File implements Node {
  """The ID of an object"""
  id: ID!
  name: String!
  projectID: ID!
  index: Int!
  text: TextContent!
  files: [File!]!
}

type FileRemoveEvent {
  fileID: ID!
}

type Mutation {
  addFile(input: AddFileInput!): AddFilePayload
  renameFile(input: RenameFileInput!): RenameFilePayload
  removeFile(input: RemoveFileInput!): RemoveFilePayload
  saveFile(input: SaveFileInput!): SaveFilePayload
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

type Project implements Node {
  """The ID of an object"""
  id: ID!
  rootFolderID: ID!
  rootFolder: File!
}

type Query {
  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
  appState: AppState!
  project(
    """The ID of an object"""
    id: ID!
  ): Project
  file(
    """The ID of an object"""
    id: ID!
  ): File
}

input RemoveFileInput {
  id: ID!
  parentID: ID!
  clientMutationId: String
}

type RemoveFilePayload {
  parent: File
  clientMutationId: String
}

input RenameFileInput {
  id: ID!
  name: String!
  clientMutationId: String
}

type RenameFilePayload {
  file: File!
  clientMutationId: String
}

input SaveFileInput {
  id: ID!
  text: TextContent!
  clientMutationId: String
}

type SaveFilePayload {
  file: File
  clientMutationId: String
}

type Subscription {
  fileRemoveNotification: FileRemoveEvent
}

"""a text content"""
scalar TextContent
