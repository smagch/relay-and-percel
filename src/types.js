// @flow

export type ID = string;

export type File = {
  id: ID,
  name: string,
  text: string,
};

export type Disposable = {
  dispose: () => void;
}

// TODO
export type DB = any;
export type Column = any;