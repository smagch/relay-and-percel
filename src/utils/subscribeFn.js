
import { parse, subscribe } from 'graphql';
import { schema } from '../schema';

export class Disposable {
  constructor() {
    this._disposed = false;
    this.subscription = null;
  }
  dispose() {
    this._disposed = true;
    if (this.subscription) {
      this.subscription.return();
    }
  }
  async setupSubscribe(operation, variables, context, observer) {
    try {
      const ast = parse(operation.text);
      const subscription = await subscribe(
        schema,
        ast,
        null,
        context,
        variables,
        operation.name
      );
      if (this._disposed) {
        return;
      }
      console.log('subscribed:');
      this.subscription = subscription;
      for await (let val of subscription) {
        if (this._disposed) {
         return;
        }
        observer.onNext(val);
      }
      observer.onCompleted();
    } catch (err) {
      if (!this._disposed) {
        observer.onError(err);
      }
    } finally {
      this.subscription = null;
    }
  }
}

const subscribeFn = context =>
  (operation, variables, cacheConfig, observer) => {
    const disposable = new Disposable();
    disposable.setupSubscribe(operation, variables, context, observer);
    return disposable;
  };

export default subscribeFn;

