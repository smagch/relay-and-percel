// @flow

import React from 'react';

type Props = {
  active: boolean,
  style: string,
  onToggle: (style: string) => void,
  className: string,
};

class StyleButton extends React.Component<Props> {

  onToggle = (e) => {
    e.preventDefault();
    this.props.onToggle(this.props.style);
  }

  render() {
    const props = this.props;
    let className = props.className;
    if (props.active) {
      className += ' active';
    }
    return (
      <span className={className} onMouseDown={this.onToggle}>
        {this.props.children}
      </span>
    );
  }
}

export default StyleButton;