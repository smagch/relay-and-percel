// @flow

import React from 'react'
import DialogBase from './DialogBase';
import type { Props } from './DialogBase';

export type SaveAsDialogProps = $Diff<Props, {
  label: string,
  actionName: string,
}>;

export const SaveAsDialog = (props: SaveAsDialogProps) => {
  return (
    <DialogBase
      {...props}
      label="Save File As"
      actionName="Save"
    />
  );
}
