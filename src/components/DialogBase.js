// @flow

import React from 'react'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import Dialog, {
  DialogActions,
  DialogContent,
} from 'material-ui/Dialog'

export type Props = {
  defaultValue: string,
  label: string,
  actionName: string,
  confirm: (text: string) => void,
  cancel: () => void,
};

export default class DialogBase extends React.Component<Props> {

  inputRef: ?HTMLInputElement;

  refCallback = (ref: ?HTMLInputElement) => {
    this.inputRef = ref;
    if (ref) {
      ref.setSelectionRange(0, ref.value.length);
    }
  }

  confirm = () => {
    if (this.inputRef) {
      this.props.confirm(this.inputRef.value);
    } else {
      throw new Error('no input ref');
    }
  }

  onKeydown = (e: KeyboardEvent) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.confirm();
    }
  }

  render() {
    return (
      <Dialog
        open={true}
        onEscapeKeyDown={this.props.cancel}
        aria-labelledby="form-dialog-title"
      >
        <DialogContent>
          <TextField
            autoFocus={true}
            margin="dense"
            label={this.props.label}
            type="text"
            defaultValue={this.props.defaultValue}
            fullWidth={true}
            inputRef={this.refCallback}
            InputProps={{ onKeyDown: this.onKeydown }}
          />
  
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.cancel} color="primary">
            Cancel
          </Button>
          <Button onClick={this.confirm} color="primary">
            {this.props.actionName}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
