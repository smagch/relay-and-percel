// @flow

import React from 'react'
import DialogBase from './DialogBase';
import type { Props } from './DialogBase';

export type RenameDialogProps = $Diff<Props, {
  label: string,
  actionName: string,
}>;

export const RenameDialog = (props: RenameDialogProps) => {
  return (
    <DialogBase
      {...props}
      label="New Filename"
      actionName="Rename"
    />
  );
}
