// @flow

import React from 'react';
import {
  Editor,
  EditorState,
  KeyBindingUtil,
  getDefaultKeyBinding,
  ContentState,
  RichUtils,
} from 'draft-js';
import Divider from 'material-ui/Divider';
import { withStyles } from 'material-ui/styles';

import InlineStyleControls from './InlineStyleControls';
import BlockStyleControls from './BlockStyleControls';

type Props = {
  previousContent?: ContentState,
  editorState: EditorState,
  onSave: (contentState: ContentState) => void,
  onTextChange: (changed: boolean) => void,
};

type State = {
  editorState: EditorState,
};

const styles = (theme) => ({
  root: {
    padding: theme.spacing.unit,
  },
  inlines: {
    '&.active': {
      color: theme.palette.primary.main,
    },
    display: 'inline-block',
  },
  blocks: {
    '&.active': {
      color: theme.palette.primary.main,
    },
    display: 'inline-block',
    marginLeft: theme.spacing.unit * 4,
  },
  button: {
    color: theme.palette.text.secondary,
    '&.active': {
      color: '#2196F3',
    },
    cursor: 'pointer',
  },
});

class RichEditor extends React.Component<Props, State> {

  editorRef: ?Editor;
  toFocus: boolean = false;
  fileChanged: boolean = false;

  constructor(props: Props) {
    super(props);
    this.state = {
      editorState: props.editorState,
    };
  }

  getEditorState(): EditorState {
    return this.state.editorState;
  }

  handleRef = (ref: Editor) => {
    this.editorRef = ref;
  }

  onChange = (editorState: EditorState) => {
    const fileChanged = this.isFileChanged(editorState, this.props.previousContent);
    if (fileChanged !== this.fileChanged) {
      this.fileChanged = fileChanged;
      this.props.onTextChange(fileChanged);
    }
    this.setState({ editorState });
  }

  focus = () => {
    if (this.editorRef) {
      this.editorRef.focus();
    }
  }

  focusToLast() {
    const { editorState } = this.state;
    const newEditorState =  EditorState.moveFocusToEnd(editorState);
    this.setState({
      editorState: newEditorState,
    });
  }

  isFileChanged(editorState: EditorState, contentState?: ContentState) {
    if (!contentState) {
      return editorState.getCurrentContent().hasText();
    }
    return !editorState.getCurrentContent().equals(contentState);
  }

  keyBinding = (e: SyntheticKeyboardEvent<>) => {
    console.log('keyBinding: ', e);
    if (e.keyCode === 83 && KeyBindingUtil.hasCommandModifier(e)) {
      e.preventDefault();
      return 'myeditor-save';
    }
    if (e.keyCode === 9 /* TAB */) {
      this.handleTab(e);
    }
    return getDefaultKeyBinding(e)
  }

  handleTab = (e: SyntheticKeyboardEvent<>) => {
    e.preventDefault();
    console.log('tab');
    const { editorState } = this.state;
    const newEditorState = RichUtils.onTab(
      e,
      editorState,
      4, /* maxDepth */
    );
    if (newEditorState !== editorState) {
      this.onChange(newEditorState);
    }
    return;
  }

  handleKeyCommand = (command: string) => {
    const { editorState } = this.state;
    if (command === 'myeditor-save') {
      const content = editorState.getCurrentContent();
      this.props.onSave(content);
      return 'handled';
    }
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }

  componentWillReceiveProps(nextProps: Props) {
    const { editorState } = nextProps;
    this.setState({ editorState });
    this.fileChanged = this.isFileChanged(editorState, nextProps.previousContent);
    this.props.onTextChange(this.fileChanged);
    this.toFocus = true;
  }

  componentDidUpdate() {
    if (this.toFocus) {
      this.focus();
      this.toFocus = false;
    }
  }

  toggleBlockType = (blockType) => {
    this.onChange(
      RichUtils.toggleBlockType(
        this.state.editorState,
        blockType
      )
    );
  }

  toggleInlineStyle = (inlineStyle) => {
    this.onChange(
      RichUtils.toggleInlineStyle(
        this.state.editorState,
        inlineStyle
      )
    );
  }

  render() {
    const { editorState } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <InlineStyleControls
          editorState={editorState}
          onToggle={this.toggleInlineStyle}
          classes={{root: classes.inlines, button: classes.button }}
        />
        <BlockStyleControls
          editorState={editorState}
          onToggle={this.toggleBlockType}
          classes={{root: classes.blocks, button: classes.button }}
        />
        <Divider />
        <Editor
          editorState={editorState}
          ref={this.handleRef}
          onChange={this.onChange}
          keyBindingFn={this.keyBinding}
          handleKeyCommand={this.handleKeyCommand}
          onTab={this.handleTab}
        />
      </div>
    );
  }
}

export default withStyles(styles)(RichEditor);
