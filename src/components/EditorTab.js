// @flow

import React from 'react';
import ClearIcon from 'material-ui-icons/Clear';
import Button from 'material-ui/Button';
import FiberManualRecordIcon from 'material-ui-icons/FiberManualRecord';
import { withStyles } from 'material-ui/styles';

import type { ID } from '../types';
import type { Theme } from 'material-ui/styles';

type Props = {
  value: ID,
  label: string,
  active: boolean,
  hasChange: boolean,
  onClick: (id: ID) => void,
  onClose: (id: ID) => void,
  classes: { [key: string]: any },
};

const style = (theme: Theme) => ({
  button: {
    borderRadius: 0,
    textTransform: 'none',
    minHeight: 46,
    minWidth: 120,
    fontSize: 16,
    justifyContent: 'space-between',
    borderRight: '1px solid white',
    '&, &:hover': {
      backgroundColor: theme.palette.grey[300],
    },
    '&.active': {
      backgroundColor: theme.palette.primary.contrastText,
    },
    '&:hover $clearIcon, &.active $clearIcon': {
      opacity: 1,
      visibility: 'visible',
    },
  },
  icon: {
    marginLeft: theme.spacing.unit * 1.6,
    lineHeight: 1,
  },
  clearIcon: {
    opacity: 0,
    fontSize: 16,
    visibility: 'hidden',
  },
  recordIcon: {
    fontSize: 16,
  }
});

class EditorTab extends React.Component<Props> {
  click = (e: MouseEvent) => {
    e.stopPropagation();
    this.props.onClick(this.props.value);
  }
  close = (e: MouseEvent) => {
    e.stopPropagation();
    this.props.onClose(this.props.value);
  }
  render() {
    const { classes } = this.props;

    const className = this.props.active ? 'active' : '';
    return (
      <Button
        className={className}
        classes={{ root: classes.button }}
        variant="flat"
        onClick={this.click}
        disableRipple={true}
      >
        {this.props.label}
        <div className={classes.icon}>
          {this.props.hasChange ? (
            <FiberManualRecordIcon
              className={classes.recordIcon}
            />
          ) : (
            <ClearIcon
              className={classes.clearIcon}
              onClick={this.close}
            />
          )}
        </div>
      </Button>
    );
  }
}

export default withStyles(style)(EditorTab);
