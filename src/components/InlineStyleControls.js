// @flow

import React from 'react';
import FormatBold from "material-ui-icons/FormatBold";
import FormatItalic from 'material-ui-icons/FormatItalic';
import FormatUnderline from 'material-ui-icons/FormatUnderlined';
import { withStyles } from 'material-ui/styles';

import StyleButton from "./StyleButton";

import type { EditorState } from 'draft-js';

const inclineStyles = [
  {
    label: 'Bold',
    style: 'BOLD',
    Component: FormatBold
  },
  {
    label: 'Italic',
    style: 'ITALIC',
    Component: FormatItalic
  },
  {
    label: 'Underline',
    style: 'UNDERLINE',
    Component: FormatUnderline
  },
];

type Props = {
  editorState: EditorState,
  onToggle: (style: string) => void,
};

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    '&.active $icon': {
      color: theme.palette.primary.main,
    },
  },
  icon: {
    color: 'blue',
    fontSize: 20,
  },
  button: {

  },
});

class InlineStyleControls extends React.Component<Props> {
  render() {
    const { props } = this;
    const { classes } = props;
    const currentStyle = props.editorState.getCurrentInlineStyle();

    return (
      <div className={props.classes.root}>
        {inclineStyles.map((type) =>
          <StyleButton
            key={type.label}
            active={currentStyle.has(type.style)}
            label={type.label}
            onToggle={props.onToggle}
            style={type.style}
            className={classes.button}
          >
            <type.Component />
          </StyleButton>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(InlineStyleControls);