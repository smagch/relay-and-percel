// @flow

import React from 'react';
import FormatListBulleted from 'material-ui-icons/FormatListBulleted';
import FormatListNumbered from 'material-ui-icons/FormatListNumbered';
import { withStyles } from 'material-ui/styles';

import StyleButton from './StyleButton';
import type { EditorState } from 'draft-js';

const BLOCK_TYPES = [
  // {label: 'H1', style: 'header-one'},
  // {label: 'H2', style: 'header-two'},
  // {label: 'H3', style: 'header-three'},
  // {label: 'H4', style: 'header-four'},
  // {label: 'Blockquote', style: 'blockquote'},
  {label: 'UL', style: 'unordered-list-item', Component: FormatListBulleted },
  {label: 'OL', style: 'ordered-list-item', Component: FormatListNumbered },
];

type Props = {
  editorState: EditorState,
  onToggle: (style: string) => void,
};

const styles = (theme) => ({
  root: {
    // '&.active': {
    //   color: theme.palette.primary.main,
    // },
  },
  button: {

  },
});

class BlockStyleControls extends React.Component<Props> {
  render() {
    const { editorState, classes } = this.props;
    const selection = editorState.getSelection();
    const blockType = editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();

    return (
      <div className={classes.root}>
        {BLOCK_TYPES.map((type) =>
          <StyleButton
            key={type.label}
            active={type.style === blockType}
            onToggle={this.props.onToggle}
            style={type.style}
            className={classes.button}
          >
            {type.Component ? (
              <type.Component />
            ) : (
              type.label
            )}
          </StyleButton>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(BlockStyleControls);
