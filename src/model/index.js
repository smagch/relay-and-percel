// @flow

import lf from 'lovefield';
import type { DB } from '../types';

import FileModel from './file';
import ProjectModel from './project';

export default (db: DB) => {
  const file = FileModel(db);
  const project = ProjectModel(db);
  return {
    ...file,
    ...project,
  };
};

function buildSchema(schemaBuilder) {

  schemaBuilder.createTable('FileEntry')
    .addColumn('id', lf.Type.STRING)
    .addColumn('name', lf.Type.STRING)
    .addColumn('parentID', lf.Type.STRING)
    .addColumn('index', lf.Type.INTEGER)
    .addColumn('content', lf.Type.OBJECT)
    .addPrimaryKey(['id'])
    .addNullable(['parentID'])
    .addUnique('idx_parentID_index', ['parentID', 'index'])
    .addForeignKey('fk_parentID', {
      local: 'parentID',
      ref: 'FileEntry.id',
      // if you use CASCADE, it thows strange errors.
      action: lf.ConstraintAction.RESTRICT,
    });

  schemaBuilder.createTable('Project')
    .addColumn('id', lf.Type.STRING)
    .addColumn('name', lf.Type.STRING)
    .addColumn('rootFolderID', lf.Type.STRING)
    .addPrimaryKey(['id'])
    .addForeignKey('fk_rootFolder', {
      local: 'rootFolderID',
      ref: 'FileEntry.id',
      action: lf.ConstraintAction.RESTRICT,
    });

  return schemaBuilder;
}

function getDataType() {
  return process.env.NODE_ENV === 'production'
    ? lf.schema.DataStoreType.INDEXED_DB
    : lf.schema.DataStoreType.MEMORY;
}

export async function initDatabase(): DB {
  const storeType = getDataType();
  // const storeType = lf.schema.DataStoreType.INDEXED_DB;
  let schemaBuilder = lf.schema.create('EE2', 1);
  schemaBuilder = buildSchema(schemaBuilder);
  const db = await schemaBuilder.connect({ storeType });
  return db;
}
