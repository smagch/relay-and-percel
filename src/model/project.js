
import { generate } from 'shortid';

// TODO
const FolderType = 2;

export default (db) => {

  async function getProject(projectID) {
    const p = db.getSchema().table('Project');
    const rows = await db.select().from(p).where(p.id.eq(projectID)).exec();
    return rows[0];
  }

  async function pickProject() {
    const t = db.getSchema().table('Project');
    const rows = await db.select().from(t).limit(1).exec();
    return rows[0];
  }

  async function createProject(projectID, projectName) {
    const p = db.getSchema().table('Project');
    const f = db.getSchema().table('FileEntry');
    const tx = db.createTransaction();
    try {
      await tx.begin([p, f]);
      const projects = await tx.attach(db.select(p.id).from(p));
      const nextIndex = projects.length;

      const rootFolderID = generate();
      const rootFolder = f.createRow({
        id: rootFolderID,
        name: 'rootFolder',
        index: nextIndex,
        type: FolderType,
        content: null,
        parentID: null,
      });
      await tx.attach(db.insertOrReplace().into(f).values([rootFolder]));

      const projectRow = p.createRow({
        id: projectID,
        name: projectName,
        rootFolderID: rootFolderID,
      });

      const projectRows = await tx.attach(db.insertOrReplace().into(p).values([projectRow]));

      await tx.commit();
      return projectRows[0];
    } catch(txError) {
      tx.rollback();
      throw txError;
    }
  }

  return {
    getProject,
    pickProject,
    createProject,
  }
}
