
import { initDatabase } from '.';
import ProjectModel from './project';

let db;
let model;

beforeEach(async () => {
  db = await initDatabase();
  model = ProjectModel(db);
});

test('create a project', async () => {
  const project = await model.createProject('Project ID', 'Project Name');
  const f = db.getSchema().table('FileEntry');
  const files = await db.select(f.id).from(f).exec();
  expect(files).toHaveLength(1);
  expect(project).toEqual({
    id: 'Project ID',
    name: 'Project Name',
    rootFolderID: files[0].id,
  });

  const got = await model.getProject(project.id);
  expect(got).toEqual(project);
});

test('create multiple projects', async () => {
  const f = db.getSchema().table('FileEntry');
  const project1 = await model.createProject('one', 'First Project');
  const [rootFolder1] = await db.select().from(f).exec();
  const project2 = await model.createProject('two', 'Second Project');
  const files = await db.select().from(f).exec();

  expect(files).toHaveLength(2);
  const rootFolder2 = files.find(file => file.id !== rootFolder1.id);

  expect(project1).toEqual({
    id: 'one',
    name: 'First Project',
    rootFolderID: rootFolder1.id,
  })
  expect(project2).toMatchObject({
    id: 'two',
    name: 'Second Project',
    rootFolderID: rootFolder2.id,
  })
});
