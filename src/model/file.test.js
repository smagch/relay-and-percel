// @flow

import FileModel from './file';

import { initDatabase } from '.';

let db;
let root;
let parentID;
let model;

beforeEach(async () => {
  db = await initDatabase();
  const t = db.getSchema().table('FileEntry');
  const row = t.createRow({
    id: 'rootid',
    name: 'rootFile',
    index: 0,
    content: null,
    parentID: null,
  });

  const rows = await db.insertOrReplace().into(t).values([row]).exec();
  root = rows[0];
  parentID = root.id;
  model = FileModel(db);
});

test('default files', async () => {
  const files = await model.listFiles(root.id);
  expect(files).toEqual([]);
});

test('insert into an invalid index', async () => {
  const promise = model.listFiles(root.id, [{ id: 'id', name: 'name'}], 1);
  expect(promise).rejects.toThrow('index out of range: 1');
});

test('append a file', async () => {
  const filesToInsert2 = {
    id: 'foobarid',
    name: 'foobar',
  };
  const files = await model.insertFiles(parentID, [filesToInsert2]);
  expect(files).toEqual([
    {
      id: 'foobarid',
      name: 'foobar',
      index: 0,
      parentID,
      content: null,
    },
  ]);
});

test('append files', async () => {
  const filesToInsert = [
    {
      id: 'fooid',
      name: 'foo',
    },
    {
      id: 'barid',
      name: 'bar',
    },
  ];
  const files = await model.insertFiles(parentID, filesToInsert);
  expect(files).toEqual([
    {
      id: 'fooid',
      name: 'foo',
      index: 0,
      parentID,
      content: null,
    },
    {
      id: 'barid',
      name: 'bar',
      index: 1,
      parentID,
      content: null,
    },
  ]);
});

test('insert a file in the middle', async () => {
  await model.insertFiles(parentID, [
    {
      id: '1',
      name: 'one',
    },
    {
      id: '2',
      name: 'two',
    },
  ]);

  const files = await model.insertFiles(parentID, [
    {
      id: '3',
      name: 'three',
    },
  ], 1);

  expect(files).toEqual([
    {
      id: '1',
      name: 'one',
      parentID,
      index: 0,
      content: null,
    },
    {
      id: '3',
      name: 'three',
      parentID,
      index: 1,
      content: null,
    },
    {
      id: '2',
      name: 'two',
      parentID,
      index: 2,
      content: null,
    },
  ]);
});

test('insert files in the middle', async () => {
  await model.insertFiles(parentID, [
    {
      id: '1',
      name: 'one',
    },
  ]);

  const files = await model.insertFiles(parentID, [
    {
      id: '2',
      name: 'two',
    },
    {
      id: '3',
      name: 'three',
    },
  ], 0);

  expect(files).toEqual([
    {
      id: '2',
      name: 'two',
      parentID,
      index: 0,
      content: null,
    },
    {
      id: '3',
      name: 'three',
      parentID,
      index: 1,
      content: null,
    },
    {
      id: '1',
      name: 'one',
      parentID,
      index: 2,
      content: null,
    },
  ]);
});

test('rename a file', async () => {
  await model.renameFile(parentID, 'Renamed Root Folder');
  const t = db.getSchema().table('FileEntry')
  const rootFolder = await db.select().from(t).where(t.id.eq(parentID)).exec();
  expect(rootFolder[0].name).toBe('Renamed Root Folder');
});

// test('create a text', async () => {
//   await insertFiles(db, parentID, [
//     {
//       id: 'file1',
//       name: 'one',
//     },
//   ]);

//   let textContent = await getTextContent(db, 'file1');
//   expect(textContent).toBeFalsy();

//   const content = { blocks: {} };
//   textContent = await createTextContent(db, 'file1', content);
//   expect(textContent).toEqual({
//     id: 1,
//     fileID: 'file1',
//     content: content,
//   });
// });