// @flow

import { execute } from './dbutils';

import type { ID, DB } from '../types';

type FileInput = {
  id: ID,
  name: string,
  content: any,
};

export default (db: DB) => {

  function listFiles(
    parentID: ID,
    ...columns: Array<Column>
  ) {
    const t = db.getSchema().table('FileEntry');
    return db.select(...columns).from(t)
      .where(t.parentID.eq(parentID))
      .orderBy(t.index)
      .exec();
  }

  async function getFile(
    id: ID,
  ) {
    const file = db.getSchema().table('FileEntry');
    const rows = await db.select().from(file).where(file.id.eq(id)).exec();
    return rows[0];
  }

  async function renameFile(
    fileID: ID,
    filename: string,
  ) {
    const t = db.getSchema().table('FileEntry');
    const rows = await db.update(t)
      .set(t.name, filename).where(t.id.eq(fileID)).exec();
    // console.log('rows: ', rows);
    return rows[0];
  }

  async function insertFiles(
    parentID: ID,
    files: Array<FileInput>,
    indexToInsert: ?number,
  ): Array<any> {
    const t = db.getSchema().table('FileEntry');
    const rows = await listFiles(parentID, t.id, t.index);
    if (indexToInsert === undefined) {
      indexToInsert = rows.length;
    }
    if (rows.length < indexToInsert) {
      throw new Error(`index out of range: ${indexToInsert}`);
    }
    const queries = [];
  
    // update index first
    if (indexToInsert !== rows.length) {
      const toUpdate = rows.filter((row, i) => {
        return i >= indexToInsert;
      });
      const increment = files.length;
      for (const row of toUpdate) {
        const q = db.update(t)
          .set(t.index, row.index + increment)
          .where(t.id.eq(row.id));
        queries.push(q);
      }
    }
    const rowsToInsert = [];
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const fileRow = t.createRow({
        id: file.id,
        name: file.name,
        content: file.content || null,
        parentID,
        index: indexToInsert + i,
      });
      rowsToInsert.push(fileRow);
    }
    queries.push(db.insertOrReplace().into(t).values(rowsToInsert));

    await execute(db, t, queries);

    return listFiles(parentID);
  }

  async function removeFiles(
    parentID: ID,
    ids: Array<ID>,
  ) {
    const t = db.getSchema().table('FileEntry');
    let rows = await listFiles(parentID, t.id, t.index);
    if (ids.length === 0) {
      throw new Error('empty ids');
    }
    rows = rows.slice();
    const queries = [];
    let index = 0;
    for (const row of rows) {
      const toRemove = ids.some(id => id === row.id);
      if (toRemove) {
        const deleteQuery = db.delete().from(t).where(t.id.eq(row.id));
        queries.push(deleteQuery);
      } else {
        if (queries.length) {
          // Update index only if there is at least one removed item
          const updateQuery = db.update(t).set(t.index, index).where(t.id.eq(row.id));
          queries.push(updateQuery);
        }
        index++;
      }
    }

    await execute(db, t, queries);
    return listFiles(parentID);
  }

  async function saveTextContent(
    fileID: ID,
    textContent: any,
  ) {
    const t = db.getSchema().table('FileEntry');
    const rows = await db.update(t)
      .set(t.content, textContent).where(t.id.eq(fileID)).exec();
    return rows[0];
  }

  return {
    listFiles,
    getFile,
    insertFiles,
    removeFiles,
    renameFile,
    saveTextContent,
  };
};
