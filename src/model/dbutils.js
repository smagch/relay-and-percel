
export async function execute(db, t, queries) {
  if (queries.length === 0) {
    return;
  }
  if (queries.length === 1) {
    return queries[0].exec();
  }

  const tx = db.createTransaction();
  try {
    await tx.begin([t]);
    for (const q of queries) {
      await tx.attach(q);
    }
    await tx.commit();
  } catch(txError) {
    await tx.rollback();
    throw txError;
  }
}
