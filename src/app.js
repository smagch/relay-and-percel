
import React from "react";
import ReactDOM from "react-dom";
import { graphql as ql } from 'graphql';
import { QueryRenderer, graphql } from 'react-relay';

import {
  Environment,
  Network,
  RecordSource,
  Store,
} from 'relay-runtime';

import EventEmitter from 'events';

import { schema } from './schema';
import { initDatabase } from './model';
import subscribeFn from './utils/subscribeFn';
import App from './containers/App';
import Model from './model';

const fetchQuery = context => (operation, variables) => {
  console.log('fetchQuery: ', operation, variables);
  return ql(
    schema,
    operation.text,
    null,
    context,
    variables,
    operation.name,
  ).then(res => {
    console.log('got response: ', res);
    return res;
  });
}

const mountNode = document.getElementById("app");

main().catch(err => {
  console.error(err);
});

async function main() {
  const db = await initDatabase();
  const model = Model(db);
  const context = {
    db,
    model,
    emitter: new EventEmitter(),
  };
  const fetcher = fetchQuery(context);
  const subscriber = subscribeFn(context);
  const environment = new Environment({
    network: Network.create(fetcher, subscriber),
    store: new Store(new RecordSource()),
  });
  environment.emitter = new EventEmitter();

  ReactDOM.render(
    <QueryRenderer
      environment={environment}
      query={graphql`
        query appQuery {
          appState {
            ...App_appState
          }
        }
      `}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>;
        } else if (props) {
          return <App appState={props.appState} />;
        }
        return <div>Loading</div>;
      }}
    />,
    mountNode,
  );

  window.environment = environment;
}
