// @flow

import React from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import { withStyles } from 'material-ui/styles';

import EditorTab from '../components/EditorTab';
import commitSelect from '../mutations/SelectFile';
import commitClose from '../mutations/CloseFile';
import commitAddTemporaryFile from '../mutations/AddTemporaryFile';

import type {RelayProp} from 'react-relay';
import type {EditorTabs_files} from './__generated__/EditorTabs_files.graphql';

type Props = {
  files: EditorTabs_files,
  relay: RelayProp,
  classes: { [key: 'bar' | 'openButton']: any },
}

const OpenButton = (props) => (
  <Button
    variant="flat"
    onClick={props.onClick}
    disableRipple={true}
    className={props.className}
  >
    <AddIcon style={{fontSize: 18}}/>
  </Button>
);

const styles = (theme) => ({
  bar: {
    minHeight: 46,
    backgroundColor: theme.palette.grey[300],
  },
  openButton: {
    minHeight: 46,
  },
});

class EditorTabs extends React.Component<Props> {

  handleClose = (fileID) => {
    // TODO: dialog to save
    commitClose(this.props.relay.environment, fileID);
  }

  getActiveID() {
    const activeFile = this.props.files.find(f => f.selected);
    return activeFile ? activeFile.id : null;
  }

  handleClickTab = (fileID) => {
    if (this.getActiveID() !== fileID) {
      commitSelect(this.props.relay.environment, fileID);
    }
  }

  handleAddFile = () => {
    commitAddTemporaryFile(
      this.props.relay.environment
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <AppBar
        className={classes.bar}
        position="static"
        elevation={0}
      >
        <div>
          {this.props.files.map(file => {
            const hasChange = !!file.hasChange;
            const selected = !!file.selected;
            return (
              <EditorTab
                key={file.id}
                value={file.id}
                label={file.name}
                active={selected}
                onClick={this.handleClickTab}
                onClose={this.handleClose}
                hasChange={hasChange}
              />
            );
          })}
          <OpenButton
            key="openbutton"
            className={classes.openButton}
            onClick={this.handleAddFile}
          />
        </div>
      </AppBar>
    );
  }
}

export default createFragmentContainer(
  withStyles(styles)(EditorTabs),
  graphql`
    fragment EditorTabs_files on File @relay(plural: true) {
      id
      name
      hasChange
      selected
    }
  `,
);
