// @flow

import React from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import { withStyles } from 'material-ui/styles';

import TextEditor from './TextEditor';
import EditorTabs from './EditorTabs';
import { SaveAsDialog } from '../components/SaveAsDialog';
import commitSaveFile from '../mutations/SaveFile';
import commitAddFile from '../mutations/AddFile';

import type { ContentState } from 'draft-js';
import type { RelayProp } from 'react-relay';
import type { Theme } from 'material-ui/styles';

type Props = {
  relay: RelayProp,
  appState: any,
  classes: { [key: string]: any },
};

type State = {
  contentToSave: ContentState | null,
}

const styles = (theme: Theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
    backgroundColor: 'white',
  },
  editor: {
    flex: 1,
    position: 'relative',
    overflow: 'scroll',
  },
});

class EditorFrame extends React.Component<Props, State> {
  //
  state = {
    contentToSave: null,
  }

  handleDialogClose = () => {
    this.setState({ contentToSave: null });
  }

  handleSaveNewFile = (filename: string) => {
    const { contentToSave } = this.state;
    if (!contentToSave) {
      throw new Error('no content to save');
    }
    const { activeFile, currentProject } = this.props.appState;
    commitAddFile(
      this.props.relay.environment,
      currentProject.id,
      activeFile.id,
      filename,
      contentToSave,
    );
    this.handleDialogClose();
  }

  handleSave = (content: ContentState) => {
    const { activeFile } = this.props.appState;
    if (activeFile.local) {
      this.setState({ contentToSave: content });
    } else {
      commitSaveFile(
        this.props.relay.environment,
        activeFile.id,
        content,
      );
    }
  }

  render() {
    const { appState = {}, classes } = this.props;
    const { openingFiles, activeFile } = appState;
    return (
      <div className={classes.root}>
        {this.state.contentToSave &&
          <SaveAsDialog
            defaultValue={activeFile.name}
            confirm={this.handleSaveNewFile}
            cancel={this.handleDialogClose}
          />
        }
        <EditorTabs
          files={openingFiles}
        />
        {activeFile &&
          <TextEditor
            file={activeFile}
            onSave={this.handleSave}
            classes={{ root: classes.editor }}
          />
        }
      </div>
    );
  }
}

export default createFragmentContainer(
  withStyles(styles)(EditorFrame),
  graphql`
    fragment EditorFrame_appState on AppState {
      currentProject {
        id
      }
      openingFiles {
        ...EditorTabs_files
      }
      activeFile {
        id
        name
        local
        ...TextEditor_file
      }
    }
  `
);
