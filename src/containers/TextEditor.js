// @flow

import React from 'react';
import {
  graphql,
  createRefetchContainer,
} from 'react-relay';
import {
  EditorState,
  ContentState,
} from 'draft-js';
import { withStyles } from 'material-ui/styles';

import commitUpdateFileState from '../mutations/UpdateFileState';
import { subscribe } from '../mutations/CloseFile';
import { subscribeFocus } from '../mutations/SelectFile';
import RichEditor from '../components/RichEditor';

import type { Editor } from 'draft-js';
import type { RelayProp } from 'react-relay';
import type { ID, Disposable } from '../types';
import type { TextEditor_file } from './__generated__/TextEditor_file.graphql';

type Props = {
  relay: RelayProp,
  file: TextEditor_file,
  onSave: (contentState: ContentState) => void,
  classes: { [key: 'root']: any },
};

const styles = (theme) => ({
  root: {
    backgroundColor: theme.palette.primary.contrastText,
    fontSize: 18,
  },
});

class TextEditor extends React.Component<Props> {
  
  editorStates: {
    [id: string]: EditorState,
  } = {};
  editorRef: ?Editor;
  disposable: ?Disposable;
  disposable2: ?Disposable;

  constructor(props) {
    super(props);
    const { file } = props;
    if (!file.text && !file.local) {
      this._refetch(file.id);
    } else {
      const editorState = this.getEditorState(file);
      this.setEditorState(file.id, editorState);
    }
  }

  getEditorState(file) {
    return file.text
      ? EditorState.createWithContent(file.text)
      : EditorState.createEmpty();
  }

  componentDidMount() {
    const { environment } = this.props.relay;
    this.disposable = subscribe(environment, fileID => {
      delete this.editorStates[fileID];
    });
    this.disposable2 = subscribeFocus(environment, this.focus);
  }

  componentWillUnmount() {
    // this.editorStates = null;
    if (this.disposable) {
      this.disposable.dispose();
    }
    if (this.disposable2) {
      this.disposable2.dispose();
    }
    if (this.editorRef) {
      this.editorRef = null;
    }
  }

  focus = () => {
    if (this.editorRef) {
      this.editorRef.focus();
    }
  }

  handleRef = (ref: Editor) => {
    this.editorRef = ref;
  }

  setEditorState(fileID: ID, editorState: EditorState) {
    this.editorStates[fileID] = editorState;
  }

  // prepare editorState here.
  // move current editorState to state.
  // if closed
  componentWillReceiveProps(nextProps) {
    console.log('Editor will rceive props: ', nextProps.file, this.props.file);
    const { file: nextFile } = nextProps;
    if (this.editorRef) {
      // save current editorState.
      this.setEditorState(
        this.props.file.id,
        this.editorRef.getEditorState(),
      );
    }

    let editorState = this.editorStates[nextFile.id];

    // Tab clicked.
    if (editorState) {
      return;
    }

    // new File opened
    console.log('new file opened: ', this.editorStates);
    const remoteTextExists = !nextFile.text && !nextFile.local;
    if (remoteTextExists) {
      this._refetch(nextFile.id);
    } else {
      // Local File
      editorState = this.getEditorState(nextFile);
      this.setEditorState(nextFile.id, editorState);
    }
  }

  commitFileChange = (changed) => {
    commitUpdateFileState(
      this.props.relay.environment,
      this.props.file.id,
      changed
    );
  }

  focusToLast = (e) => {
    e.stopPropagation();
    if (this.editorRef) {
      this.editorRef.focusToLast();
    }
  }

  stopPropagation = (e) => {
    e.stopPropagation();
  }
  _refetch(fileID: ID) {
    this.props.relay.refetch({ fileID });
  }
  render() {
    const { file, classes } = this.props;
    const editorState = this.editorStates[file.id];

    return (
      <div className={classes.root} onClick={this.focusToLast}>
        <div onClick={this.stopPropagation}>
          {editorState &&
            <RichEditor
              editorState={editorState}
              previousContent={file.text}
              innerRef={this.handleRef}
              onSave={this.props.onSave}
              onTextChange={this.commitFileChange}
            />
          }
        </div>
      </div>
    );
  }
}

export default createRefetchContainer(
  withStyles(styles)(TextEditor),
  graphql`
    fragment TextEditor_file on File {
      id
      text
      local
    }
  `,
  graphql`
    query TextEditorRefetchQuery($fileID: ID!) {
      file(id: $fileID) {
        ...TextEditor_file
      }
    }
  `
);
