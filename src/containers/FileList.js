// @flow

import React from 'react';
import {createFragmentContainer, graphql} from 'react-relay';

import FileListItem from '../containers/FileListItem';
import commitRename from '../mutations/RenameFile';
import commitRemove from '../mutations/RemoveFile';
import commitSelect from '../mutations/SelectFile';
import type {FileList_project} from './__generated__/FileList_project.graphql';
import type {RelayProp} from 'react-relay';
import type {ID} from '../types';

type Props = {
  project: FileList_project,
  relay: RelayProp,
};

class FileList extends React.Component<Props> {

  handleRename = (fileID: ID, newFilename: string) => {
    commitRename(
      this.props.relay.environment,
      fileID,
      newFilename
    );
  }

  handleRemove = (fileID: ID) => {
    commitRemove(
      this.props.relay.environment,
      this.props.project.rootFolder.id,
      fileID
    );
  }

  handleSelect = (fileID: ID) => {
    commitSelect(
      this.props.relay.environment,
      fileID
    );
  }

  render() {
    const { project } = this.props;
    if (!project) {
      return <div>loading</div>;
    }
    const { rootFolder } = project;
    const { files } = rootFolder;

    if (!files) {
      return <div>loading</div>;
    }
    return (
      <div>
        {files.map(node => {
          return (
            <FileListItem
              key={node.id}
              file={node}
              onRename={this.handleRename}
              onRemove={this.handleRemove}
              onSelect={this.handleSelect}
            />
          );
        })}
      </div>
    );
  }
}

export default createFragmentContainer(
  FileList,
  graphql`
    fragment FileList_project on Project {
      id
      rootFolder {
        id
        files {
          id
         ...FileListItem_file
        }
      }
    }
  `,
);
