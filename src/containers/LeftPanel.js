
import React from 'react';
import { createFragmentContainer, graphql } from 'react-relay';

import FileList from './FileList';

class LeftPanel extends React.Component {
  render() {
    return (
      <div>
        <FileList
          project={this.props.appState.currentProject}
        />
      </div>
    );
  }
}

export default createFragmentContainer(
  LeftPanel,
  graphql`
    fragment LeftPanel_appState on AppState {
      currentProject {
        ...FileList_project
      }
    }
  `,
);
