// @flow

import React from 'react';
import {
  requestSubscription,
  graphql,
  createFragmentContainer,
} from 'react-relay';
import CssBaseline from 'material-ui/CssBaseline';
import Drawer from 'material-ui/Drawer';
import { withStyles } from 'material-ui/styles'

import type { RelayProps } from 'react-relay';
import type { App_appState } from './__generated__/App_appState.graphql';

import LeftPanel from './LeftPanel';
import EditorFrame from './EditorFrame';
import commitCloseFile from '../mutations/CloseFile';

const subscription = graphql`
  subscription AppSubscription {
    fileRemoveNotification {
      fileID
    }
  }
`;

type Props = {
  relay: RelayProps,
  appState: App_appState,
  classes: { [key: string]: any } ,
};

const styles = (theme) => ({
  root: {
    display: 'flex',
    // alignItems: 'stretch',
    height: '100vh',
    width: '100vw',
  },
  content: {
    flex: '1 1 auto',
  },
  drawer: {
    width: 250,
  },
  drawerPaper: {
    position: 'relative',
  },
});

class App extends React.Component<Props> {

  disposable = null;

  componentDidMount() {
    const { environment } = this.props.relay;
    this.disposable = requestSubscription(
      environment,
      {
        subscription,
        variables: {},
        onNext: (val) => {
          val = val.fileRemoveNotification;
          commitCloseFile(environment, val.fileID);
        },
        onError: error => console.error(error),
      }
    );
  }

  componentWillUnmount() {
    if (this.disposable) {
      this.disposable.dispose();
    }
  }

  render() {
    const { classes, appState } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Drawer
          variant="permanent"
          className={classes.drawer}
          classes={{paper: classes.drawerPaper}}
        >
          <LeftPanel appState={appState} />
        </Drawer>
        <div className={classes.content}>
          <EditorFrame appState={appState} />
        </div>
      </div>
    );
  }
}

export default createFragmentContainer(
  withStyles(styles)(App),
  graphql`
    fragment App_appState on AppState {
      ...EditorFrame_appState
      ...LeftPanel_appState
    }
  `,
);
