// @flow

import React from 'react';
import {createFragmentContainer, graphql} from 'react-relay';
import { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import { MoreVert } from 'material-ui-icons';
import Menu, { MenuItem } from 'material-ui/Menu';
import { withStyles, WithStyles } from 'material-ui/styles';

import { RenameDialog } from '../components/RenameDialog';
import type { ID } from '../types';
import type { FileListItem_file } from './__generated__/FileListItem_file.graphql';

// type File = {
//   id: ID,
//   name: string,
//   selected?: boolean,
// }

type Props = {
  file: FileListItem_file,
  // file: File,
  onSelect: (id: ID) => void,
  onRename: (id: ID, newName: string) => void,
  onRemove: (id: ID) => void,
};

type State = {
  menuOpened: boolean,
  dialogOpened: boolean,
};

type MenuProps = {
  iconRef: HTMLDivElement,
  rename: () => void,
  remove: () => void,
  close: () => void,
}

const styles = (theme) => ({
  item: {
    listStyle: 'none',
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
    },
    '&.selected, &.menuOpened.selected, &.selected:hover': {
      backgroundColor: theme.palette.primary.main,
    },
    '&.selected $text, &.selected:hover $menuIcon': {
      color: theme.palette.primary.contrastText,
    },
    '&.menuOpened': {
      backgroundColor: theme.palette.primary.light,
    },
    '& $menuIcon': {
      display: 'none'
    },
    '&:hover $menuIcon, &.menuOpened $menuIcon': {
      display: 'inline',
    },
  },
  menuIcon: {

  },
  text: {
    cursor: 'pointer',
    userSelect: 'none',
  },
});

type StyledProps = Props & WithStyles<'item' | 'menuIcon' | 'text'>

const FileMenu = (props: MenuProps) => (
  <Menu
    open={true}
    anchorEl={props.iconRef}
    onClose={props.close}
  >
    <MenuItem onClick={props.rename}>Rename</MenuItem>
    <MenuItem onClick={props.remove}>Delete</MenuItem>
  </Menu>
);

class FileList extends React.Component<StyledProps, State> {

  state = {
    menuOpened: false,
    dialogOpened: false,
  }

  iconRef: ?HTMLDivElement;

  clickMenu = () => {
    console.log('click menu')
    this.setState({ menuOpened: true })
  }

  handleMenuClose = () => {
    this.setState({ menuOpened: false })
  }

  handleDialogClose = () => {
    console.log('handle DialogClose')
    this.setState({
      dialogOpened: false,
      menuOpened: false,
    })
  }

  handleRename = (newName) => {
    this.handleDialogClose();
    if (newName !== this.props.file.name) {
      this.props.onRename(this.props.file.id, newName);
    }
  }

  openRenameDialog = () => {
    this.setState({
      dialogOpened: true,
    })
  }

  onSelect = () => {
    this.props.onSelect(this.props.file.id)
  }

  handleRemoveFile = () => {
    this.props.onRemove(this.props.file.id)
  }

  render() {
    const { file, classes } = this.props
    const { menuOpened } = this.state
    let itemClassName = classes.item
    if (this.props.file.selected) {
      itemClassName += ' selected'
    }
    if (menuOpened) {
      itemClassName += ' menuOpened'
    }

    return (
      <div className={itemClassName}>
        {this.state.dialogOpened &&
          <RenameDialog
            defaultValue={file.name}
            confirm={this.handleRename}
            cancel={this.handleDialogClose}
          />
        }
        <ListItem
          onClick={this.onSelect}
        >
          <ListItemText
            primary={`${file.index + 1}.  ${file.name}`}
            classes={{ primary: classes.text }}
          />
          <ListItemSecondaryAction>
            <IconButton
              aria-label="Option"
              onClick={this.clickMenu}
              className={classes.menuIcon}
            >
              <MoreVert />
              <div ref={(ref) => { this.iconRef = ref }} />
            </IconButton>
            {menuOpened && this.iconRef &&
              <FileMenu
                iconRef={this.iconRef}
                rename={this.openRenameDialog}
                close={this.handleMenuClose}
                remove={this.handleRemoveFile}
              />
            }
          </ListItemSecondaryAction>
        </ListItem>
      </div>
    )
  }
}

// export default withStyles(styles)(FileList);
export default createFragmentContainer(
  withStyles(styles)(FileList),
  graphql`
    fragment FileListItem_file on File {
      id
      name
      selected
      index
    }
  `,
);
