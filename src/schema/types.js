import {
  GraphQLScalarType,
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLID,
  GraphQLInt,
} from 'graphql';

import {
  nodeDefinitions,
  globalIdField,
  fromGlobalId,
  toGlobalId,
} from 'graphql-relay';

import { generate } from 'shortid';
import { ContentState, convertToRaw, convertFromRaw } from 'draft-js';

import { NonNullList } from './schemautils';

export const {nodeInterface, nodeField} = nodeDefinitions(
  (globalId, context, info) => {
    const {type, id} = fromGlobalId(globalId);
    console.log('node type ', type, ' id ', id);
    if (type === 'File') {
      return context.model.getFile(id);
    }
    if (type === 'Project') {
      return context.model.getProject(id);
    }
    return null;
  },
  (obj) => {
    throw new Error('obj called');
  }
);

export const TextContent = new GraphQLScalarType({
  name: 'TextContent',
  description: 'a text content',
  serialize(value) {
    // return convertFromRaw(value);
    return value;
  },
  parseValue(value) {
    return value;
    // return convertToRaw(value);
  },
  parseLiteral(astValue) {
    throw new Error('ast value: ' + astValue);
  },
});

export const GraphQLFile = new GraphQLObjectType({
  name: 'File',
  fields: () => ({
    id: globalIdField('File'),
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    projectID: {
      type: new GraphQLNonNull(GraphQLID),
      resolve: () => toGlobalId('Project', 'projectid'),
    },
    index: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    text: {
      type: new GraphQLNonNull(TextContent),
      resolve(file) {
        console.log('text resolve: ', file);
        if (file.text) {
          return file.text;
        }
        if (file.content) {
          return convertFromRaw(file.content);
        }
        return ContentState.createFromText('');
      },
    },
    files: {
      type: NonNullList(GraphQLFile),
      resolve(root, args, context) {
        return context.model.listFiles(root.id);
      },
    }
  }),
  interfaces: [nodeInterface],
});

export const GraphQLProject = new GraphQLObjectType({
  name: 'Project',
  fields: {
    id: globalIdField('Project'),
    // TODO: name
    rootFolderID: {
      type: new GraphQLNonNull(GraphQLID),
    },
    rootFolder: {
      type: new GraphQLNonNull(GraphQLFile),
      resolve(root, _, context) {
        return context.model.getFile(root.rootFolderID);
      },
    },
  },
  interfaces: [nodeInterface],
});

async function addProject(context, name = 'default project') {
  return context.model.createProject('projectid', name);
}

async function initProject(context) {
  const project = await addProject(context);
  const toInsert = ['File1', 'File2', 'File3'].map(filename => {
    const text = `text for ${filename}`;
    const contentState = ContentState.createFromText(text);
    const rawText = convertToRaw(contentState);
    return {
      id: generate(),
      name: filename,
      content: rawText,
    }
  });
  await context.model.insertFiles(project.rootFolderID, toInsert);
  return project;
}

export const AppState = new GraphQLObjectType({
  name: 'AppState',
  fields: {
    currentProject: {
      type: new GraphQLNonNull(GraphQLProject),
      async resolve(root, args, context) {
        const project = await context.model.pickProject();
        if (!project) {
          return initProject(context);
        }
        return project;
      },
    },
    openingFiles: {
      type: NonNullList(GraphQLFile),
      resolve: () => [],
    },
  },
});
