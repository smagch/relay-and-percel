
import {
  GraphQLObjectType,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  globalIdField,
} from 'graphql-relay';

import { GraphQLFile, GraphQLProject, nodeField, AppState } from './types';

export const Query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    node: nodeField,
    appState: {
      type: new GraphQLNonNull(AppState),
      resolve() {
        return {};
      },
    },
    project: {
      type: GraphQLProject,
      args: {
        id: globalIdField('Project'),
      },
      async resolve(root, { id }, context) {
        const resolved = fromGlobalId(id);
        const project = await context.model.getProject(resolved.id);
        return project;
      },
    },
    file: {
      type: GraphQLFile,
      args: {
        id: globalIdField('File'),
      },
      resolve(root, { id }, context) {
        const { id: fileID } = fromGlobalId(id);
        return context.model.getFile(fileID);
      },
    },
  },
});
