
import {
  GraphQLNonNull,
  GraphQLList,
} from 'graphql';

export const NonNullList = type => (
  new GraphQLNonNull(
    new GraphQLList(
      new GraphQLNonNull(type)
    )
  )
);
