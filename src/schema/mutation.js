import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  GraphQLFile,
  TextContent,
} from './types';

import { convertToRaw } from 'draft-js';

async function addFile({id, name, text, projectID}, model) {
  const project = await model.getProject(projectID);
  const file = {
    id,
    name,
    content: convertToRaw(text),
  };
  await model.insertFiles(project.rootFolderID, [file]);

  return {
    ...file,
    text,
    projectID,
  };
}

const AddFileMutation = mutationWithClientMutationId({
  name: 'AddFile',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    projectID: {
      type: new GraphQLNonNull(GraphQLID),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    text: {
      type: TextContent,
    },
  },
  outputFields: {
    parent: {
      type: GraphQLFile,
    },
    insertedFile: {
      type: GraphQLFile,
    },
  },
  async mutateAndGetPayload(input, context) {
    const { id } = fromGlobalId(input.id);
    const { id: projectID } = fromGlobalId(input.projectID);
    const { text: originalText } = input;
    input = {
      ...input,
      id,
      projectID,
    }
    let file = await addFile(input, context.model);
    const project = await context.model.getProject(projectID);
    let parent = await context.model.getFile(project.rootFolderID);
    file = { ...file, text: originalText };
    // parent = { ...parent, text: originalText };
    console.log('about to return parent: ', parent);
    return { parent, insertedFile: file };
  },
});

const RenameFileMutation = mutationWithClientMutationId({
  name: 'RenameFile',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  outputFields: {
    file: {
      type: new GraphQLNonNull(GraphQLFile),
    },
  },
  async mutateAndGetPayload({ id, name }, context) {
    const resolved = fromGlobalId(id);
    await context.model.renameFile(resolved.id, name);
    const file = await context.model.getFile(resolved.id);
    return { file };
  },
});

const RemoveFileMutation = mutationWithClientMutationId({
  name: 'RemoveFile',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    parentID: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  outputFields: {
    parent: {
      type: GraphQLFile,
    },
  },
  async mutateAndGetPayload({ id, parentID }, context) {
    const { id: fileID } = fromGlobalId(id);
    const { id: resolvedParentID } = fromGlobalId(parentID);
    await context.model.removeFiles(resolvedParentID, [fileID]);
    context.emitter.emit('removeFile', { fileID: id });
    console.log('resolved parent ID: ', resolvedParentID);
    const parent = await context.model.getFile(resolvedParentID);
    return { parent };
  },
});

const SaveFileMutation = mutationWithClientMutationId({
  name: 'SaveFile',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    text: {
      type: new GraphQLNonNull(TextContent),
    },
  },
  outputFields: {
    file: {
      type: GraphQLFile,
    },
  },
  async mutateAndGetPayload({ id, text }, context) {
    const { id: fileID } = fromGlobalId(id);
    const rawText = convertToRaw(text);
    await context.model.saveTextContent(fileID, rawText);
    let file = await context.model.getFile(fileID);
    file = {...file, text };
    return { file };
  },
});

export const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addFile: AddFileMutation,
    renameFile: RenameFileMutation,
    removeFile: RemoveFileMutation,
    saveFile: SaveFileMutation,
  },
});
