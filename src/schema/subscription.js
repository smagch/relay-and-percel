
import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import { $$asyncIterator } from 'iterall';

const getAsyncIterator = emitter => ({
  [$$asyncIterator]: () => ({
    next() {
      return new Promise(resolve => {
        emitter.once('removeFile', value => {
          resolve({
            value,
            done: false,
          });
        });
      });
    },
  })
});

const FileRemoveEventType = new GraphQLObjectType({
  name: 'FileRemoveEvent',
  fields: {
    fileID: { type: GraphQLNonNull(GraphQLID) },
  },
});

export const Subscription = new GraphQLObjectType({
  name: 'Subscription',
  fields: {
    fileRemoveNotification: {
      type: FileRemoveEventType,
      resolve: (val) => val,
      subscribe: (_, args, context) => {
        return getAsyncIterator(context.emitter);
      },
    },
  },
});
