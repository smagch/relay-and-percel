// @flow
import {commitMutation, graphql} from 'react-relay';
import type {Environment} from 'relay-runtime';
import type {ID} from '../types';

const mutation = graphql`
  mutation RenameFileMutation($input: RenameFileInput!) {
    renameFile(input:$input) {
      file {
        id
        name
      }
    }
  }
`;

let tempID = 1;

export default function commit(
  environment: Environment,
  id: ID,
  newFilename: string
) {
  return commitMutation(environment, {
    mutation,
    variables: {
      input: {
        id,
        name: newFilename,
        clientMutationId: '' + tempID++,
      },
    },
  });
}