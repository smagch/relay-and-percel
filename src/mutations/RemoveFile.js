// @flow

import {commitMutation, graphql} from 'react-relay';
import type {Environment} from 'relay-runtime';
import type {ID} from '../types';

const mutation = graphql`
  mutation RemoveFileMutation($input: RemoveFileInput!) {
    removeFile(input: $input) {
      parent {
        id
        files {
          id
        }
      }
    }
  }
`;

let tempID = 1;

export default function commit(
  environment: Environment,
  parentID: ID,
  fileID: ID,
) {
  return commitMutation(environment, {
    mutation,
    variables: {
      input: {
        id: fileID,
        parentID,
        clientMutationId: '' + tempID++,
      },
    },
  });
}