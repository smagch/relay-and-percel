// @flow

import {
  commitLocalUpdate,
} from 'react-relay';

import type {Environment} from 'relay-runtime';
import type {ID} from '../types';

const eventName = 'FILE_SELECTED';

export function subscribeFocus(
  environment: Environment,
  callback: (id: ID) => void,
) {
  function callbackFunc(fileID) {
    callback(fileID);
  }
  environment.emitter.on(eventName, callbackFunc);
  return {
    dispose() {
      environment.emitter.removeListener(eventName, callbackFunc);
    },
  };
}

export default function commit(
  environment: Environment,
  fileID: ID,
) {
  // 1. if already selected, focus();
  // 2
  commitLocalUpdate(environment, store => {
    console.log('selectFile called');
    const appState = store.getRoot().getOrCreateLinkedRecord('appState');
    const activeFile = appState.getLinkedRecord('activeFile');
 
    if (activeFile) {
      if (activeFile.getDataID() === fileID) {
        environment.emitter.emit(eventName, fileID);
        return;
      }
      activeFile.setValue(false, 'selected');
    }

    const file = store.get(fileID);
    file.setValue(true, 'selected');

    appState.setLinkedRecord(file, 'activeFile');
    const openingFiles = appState.getLinkedRecords('openingFiles') || [];
    if (!openingFiles.find(f => f.getDataID() === fileID)) {
      appState.setLinkedRecords(
        openingFiles.concat(file),
        'openingFiles'
      );
    }
  });
}
