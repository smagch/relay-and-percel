// @flow

import {commitMutation, graphql} from 'react-relay';
import type {Environment} from 'relay-runtime';
import type {ID} from '../types';

const mutation = graphql`
  mutation SaveFileMutation($input: SaveFileInput!) {
    saveFile(input:$input) {
      file {
        id
        text
      }
    }
  }
`;

let tempID = 1;

export default function commit(
  environment: Environment,
  fileID: ID,
  text: any,
) {
  console.log('save file commit: ', { fileID, text });
  return commitMutation(environment, {
    mutation,
    variables: {
      input: {
        id: fileID,
        text,
        clientMutationId: '' + tempID++,
      },
    },
  });
}