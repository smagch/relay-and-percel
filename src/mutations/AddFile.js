// @flow

import {commitMutation, graphql} from 'react-relay';

import type {Environment} from 'relay-runtime';
import type {ID} from '../types';

const mutation = graphql`
  mutation AddFileMutation($input: AddFileInput!) {
    addFile(input: $input) {
      parent {
        id
        files {
          id
          index
        }
      }
      insertedFile {
        id
        name
        text
      }
    }
  }
`;

let tempID = 1;

export default function commit(
  environment: Environment,
  projectID: ID,
  fileID: ID,
  filename: string,
  text: any,
) {
  return commitMutation(environment, {
    mutation,
    variables: {
      input: {
        id: fileID,
        name: filename,
        text,
        projectID,
        clientMutationId: '' + tempID++,
      },
    },
    updater: (store) => {
      const file = store.get(fileID);
      file.setValue(false, 'local');
    },
  });
}