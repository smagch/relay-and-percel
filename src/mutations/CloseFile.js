// @flow

import {
  commitLocalUpdate,
} from 'react-relay';

import type {Environment} from 'relay-runtime';
import type {ID, Disposable} from '../types';

function cleanState(fileRecord) {
  fileRecord.setValue(null, 'selected');
  fileRecord.setValue(null, 'hasChange');
  fileRecord.setValue(null, 'text');
}

function isActive(activeFile, fileID) {
  return activeFile && activeFile.getDataID() === fileID;
}

export function subscribe(
  environment: Environment,
  callback: (fileID: ID) => void
): Disposable {
  function fileClosed(fileID: ID) {
    callback(fileID)
  }
  environment.emitter.on('FILE_CLOSE', fileClosed);
  return {
    dispose() {
      environment.emitter.removeListener('FILE_CLOSE', fileClosed);
    }
  };
}

export default function commit(
  environment: Environment,
  fileID: ID,
) {
  commitLocalUpdate(environment, store => {
    const file = store.get(fileID);
    if (!file) {
      // throw new Error('attempted to close unknown file: ' + fileID);
      return;
    }
    environment.emitter.emit('FILE_CLOSE', fileID);
    cleanState(file);
    const appState = store.getRoot().getOrCreateLinkedRecord('appState');
    const activeFile = appState.getLinkedRecord('activeFile');
    const openingFiles = appState.getLinkedRecords('openingFiles');
    const filtered = openingFiles.filter(file => file.getDataID() !== fileID);
    appState.setLinkedRecords(filtered, 'openingFiles');

    if (!isActive(activeFile, fileID)) {
      return;
    }
    const currentIndex = openingFiles.findIndex(file => file.getDataID() === fileID);
    if (filtered.length) {
      const nextActive = filtered.length === currentIndex
        ? filtered[currentIndex - 1]
        : filtered[currentIndex];
      appState.setLinkedRecord(nextActive, 'activeFile');
      nextActive.setValue(true, 'selected');
    } else {
      appState.setValue(undefined, 'activeFile');
    }
  });
}
