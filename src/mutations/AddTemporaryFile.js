// @flow

import {
  commitLocalUpdate,
} from 'react-relay';
import { generate } from 'shortid';
import { toGlobalId } from 'graphql-relay';

import type {Environment} from 'relay-runtime';

function getID() {
  return toGlobalId('File', generate());
}

let tmpIndex = 1;

export default function commit(
  environment: Environment,
) {
  commitLocalUpdate(environment, store => {
    const appState = store.getRoot().getOrCreateLinkedRecord('appState');
    const openingFiles = appState.getLinkedRecords('openingFiles');
    const hasLocalFile = openingFiles.some(f => f.getValue('local'));
    if (!hasLocalFile) {
      tmpIndex = 1;
    }
    const name = `Untitled-${tmpIndex++}`;
    const id = getID();

    const file = store.create(id, 'File');
    file.setValue(id, 'id');
    file.setValue(name, 'name');
    file.setValue(true, 'selected');
    file.setValue(true, 'local');
    // due to a bug, cannot set custom scalar type
    // const text = ContentState.createFromText('');
    // file.setValue(text, 'text');

    const activeFile = appState.getLinkedRecord('activeFile');
    if (activeFile) {
      activeFile.setValue(false, 'selected');
    }
    appState.setLinkedRecords(openingFiles.concat(file), 'openingFiles');
    appState.setLinkedRecord(file, 'activeFile');
  });
}
