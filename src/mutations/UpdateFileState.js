// @flow

import {
  commitLocalUpdate,
} from 'react-relay';

import type {Environment} from 'relay-runtime';
import type {ID} from '../types';

export default function commit(
  environment: Environment,
  fileID: ID,
  value: boolean,
) {
  commitLocalUpdate(environment, store => {
    const file = store.get(fileID);
    if (file.getValue('hasChange') !== value) {
      file.setValue(value, 'hasChange');
    }
  });
}
