
import {
  subscribe,
  eventEmitterAsyncIterator,
  parse,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLBoolean,
  GraphQLInt,
  GraphQLList,
  GraphQLString,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

import { graphql as ql } from 'graphql';
import {
  Environment,
  Network,
  RecordSource,
  Store,
} from 'relay-runtime';
import {
  requestSubscription,
  graphql,
} from 'react-relay';

import { queryText } from './query';
import { $$asyncIterator } from 'iterall';
import EventEmitter from 'events';

const emitter = new EventEmitter();

class MyIter {
  //
  constructor() {

  }

  [$$asyncIterator] = function() {
    return {
      id: 1,
      next() {
        console.log('on next');
        return new Promise(resolve => {
          emitter.once('some-event', () => {
            resolve({
              value: { email: { id: this.id++, subject: 'Hello' } },
              done: false,
            });
          });
        });
      }
    };
  }
}


const EmailType = new GraphQLObjectType({
  name: 'Email',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    from: { type: GraphQLString },
    subject: { type: GraphQLString },
    message: { type: GraphQLString },
    unread: { type: GraphQLBoolean },
  },
});

const InboxType = new GraphQLObjectType({
  name: 'Inbox',
  fields: {
    total: {
      type: GraphQLInt,
      resolve: inbox => inbox.emails.length,
    },
    unread: {
      type: GraphQLInt,
      resolve: inbox => inbox.emails.filter(email => email.unread).length,
    },
    emails: { type: GraphQLList(EmailType) },
  },
});

const EmailEventType = new GraphQLObjectType({
  name: 'EmailEvent',
  fields: {
    email: { type: EmailType },
    inbox: { type: InboxType },
  },
});

const QueryType = new GraphQLObjectType({
  name: 'Query',
  fields: {
    inbox: { type: InboxType },
  },
});

const timer = (count = 1) => {
  return new Promise(resolve => {
    setTimeout(resolve, count * 1000);
  });
}

const SubscriptionType = new GraphQLObjectType({
  name: 'Subscription',
  fields: {
    importantEmail: {
      type: EmailEventType,
      // subscribe: async function* () {
      //   await timer(0.5);
      //   yield { email: { id: 1, subject: 'Hello' } };
      //   await timer(0.5);
      //   yield { email: { id: 2, subject: 'Goodbye' } };
      //   await timer(0.5);
      //   yield { email: { id: 3, subject: 'Bonjour' } };
      // }
      subscribe: () => {
        return new MyIter();
      },
      resolve: (event) => {
        console.log('resolve: ', event);
        return event;
      },
      args: {
        priority: { type: GraphQLInt },
      },
    },
  },
});

export const schema = new GraphQLSchema({
  query: QueryType,
  subscription: SubscriptionType,
});

function fetchFn(operation, variables) {
  // console.log('fetchFn: ', operation, variables);
  return ql(
    schema,
    operation.text,
    null,
    {},
    variables,
    operation.name,
  ).then(res => {
    console.log('got response: ', res);
    return res;
  });
}

class Disposable {
  constructor() {
    this._disposed = false;
    this.subscription = null;
  }
  dispose() {
    this._disposed = true;
    console.log('disposed');
    if (this.subscription) {
      this.subscription.return();
      console.log('reterned!');
    }
  }
  async setupSubscribe(operation, variables, observer) {
    try {
      const ast = parse(operation.text);
      const subscription = await subscribe(
        schema,
        ast,
        null,
        null,
        variables,
        operation.name
      );
      if (this._disposed) {
        return;
      }
      this.subscription = subscription;
      for await (let val of subscription) {
        if (this._disposed) {
          return;
        }
        observer.onNext(val);
        console.log('called onNext: ', val);
      }
      observer.onCompleted();
    } catch (err) {
      if (!this._disposed) {
        observer.onError(err);
      }
    } finally {
      this.subscription = null;
      console.log('finally!');
    }
  }
}

function subscribeFn(operation, variables, cacheConfig, observer) {
  const disposable = new Disposable();
  disposable.setupSubscribe(operation, variables, observer);
  return disposable;
}

async function main() {
  const environment = new Environment({
    network: Network.create(fetchFn, subscribeFn),
    store: new Store(new RecordSource()),
  });

  const disposable = requestSubscription(
    environment,
    {
      subscription: queryText,
      variables: {priority: 1},
      // variables: null,
      // optional but recommended:
      onCompleted: () => {
        console.log('completed!');
      },
      onNext: (val) => {
        console.log('onNext: ', val);
      },
      onError: error => {
        console.error(error);
      },
    }
  );

  setTimeout(() => {
    emitter.emit('some-event');
  }, 1000);

  setTimeout(() => {
    emitter.emit('some-event');
  }, 2000);

  setTimeout(() => {
    emitter.emit('some-event');
  }, 4000);

  setTimeout(() => {
    console.log('time elapsed');
    disposable.dispose();
  }, 3000);
}

if (require.main === module) {
  main().catch(console.error);
}

