#!/usr/bin/env babel-node

import fs from 'fs';
import path from 'path';
import { schema } from './foo';
import { printSchema } from 'graphql';

const schemaPath = path.resolve(__dirname, '../schema2.graphql');

fs.writeFileSync(schemaPath, printSchema(schema));

console.log('Wrote ' + schemaPath);
