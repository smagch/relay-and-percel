
import {
  requestSubscription,
  graphql,
} from 'react-relay';

export const queryText = graphql`
  subscription querySubscription($priority: Int) {
    importantEmail(priority: $priority) {
      email {
        subject
      }
    }
  }
`;

